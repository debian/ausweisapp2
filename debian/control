Source: ausweisapp2
Section: utils
Priority: optional
Maintainer: John Paul Adrian Glaubitz <glaubitz@physik.fu-berlin.de>
Build-Depends: cmake,
               debhelper-compat (= 12),
               libhttp-parser-dev,
               libpcsclite-dev,
               libssl-dev,
               libudev-dev,
               pkgconf,
               qt6-base-dev,
               qt6-base-private-dev,
               qt6-declarative-dev,
               qt6-scxml-dev,
               qt6-shadertools-dev,
               qt6-svg-dev,
               qt6-websockets-dev,
               qt6-tools-dev
Standards-Version: 4.6.1
Homepage: https://www.ausweisapp.bund.de
Vcs-Browser: https://salsa.debian.org/debian/ausweisapp2
Vcs-Git: https://salsa.debian.org/debian/ausweisapp2.git

Package: ausweisapp
Replaces: ausweisapp2 (<< 2.0.0-1~)
Breaks: ausweisapp2 (<< 2.0.0-1~)
Architecture: any
Depends: libqt6svg6,
         qml6-module-qt-labs-platform,
         qml6-module-qtqml,
         qml6-module-qtqml-models,
         qml6-module-qtqml-statemachine,
         qml6-module-qtqml-workerscript,
         qml6-module-qtquick-controls,
         qml6-module-qtquick-effects,
         qml6-module-qtquick-layouts,
         qml6-module-qtquick-templates,
         qml6-module-qtquick-window,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: pcsc-tools, pcscd
Description: Official authentication app for German ID cards and residence permits
 This app is developed and issued by the German government to be
 used for online authentication with electronic German ID cards
 and residence titles. To use this app, a supported RFID card
 reader or compatible NFC smart phone is required.

Package: ausweisapp2
Depends: ausweisapp, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: Transitional ausweisapp2 dummy package
 This is the transitional ausweisapp2 dummy package. It can safely be removed.
